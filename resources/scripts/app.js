var app = angular.module("SalonApp", [

    /**Directives**/
    "routes","datepicker",

    /**Controller**/
    "gblSalonCtrl","loginCtrl","dashboardCtrl","navigationCtrl","settingCtrl","userCtrl","salonServiceCtrl","branchCtrl","resourcesCtrl","assignEmployeeCtrl","assignResourceCtrl","serviceToStaffCtrl","inventoryCtrl","calendarViewCtrl","courseCtrl",  "assignmentCtrl","checkInStockCtrl", "checkOutStockCtrl","customerFeedbackCtrl","roleCtrl",

    /**Service**/
    "dateService"

]);





