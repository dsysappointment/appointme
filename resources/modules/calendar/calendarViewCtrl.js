angular.module("calendarViewCtrl",["ui.calendar"]).controller("calendarViewCtrl",["$scope","$rootScope","$filter","$window",function($scope,$rootScope,$filter,$window){

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();


    $rootScope.screenTitle = "Manage Appointment";


    $scope.events =[];

    $scope.events = [
        {title: 'Appointment1',start: new Date(y, m, 1)},
        {title: 'Appointment2',start: "2018-09-04",end: "2018-09-04"},
        {id: 999,title: 'Appointment3',start:"2018-09-02",allDay: false},
        {id: 999,title: 'Appointment4',start: new Date(y, m, d + 4, 16, 0),allDay: false},
        {title: 'Appointment5',start:"2018-09-02",allDay: false,allDay: false},
        {title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29)}
    ];

    $scope.dayClick = function(){
      
        $("#createAppointmentModal").modal("show");
        
    }

    /* alert on eventClick */
    $scope.alertOnEventClick = function( date, jsEvent, view){
        $("#updateAppointmentModal").modal('show');
    };
    /* alert on Drop */
    $scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){
        $scope.alertMessage = ('Event Droped to make dayDelta ' + delta);
    };

    /* alert on Resize */
    $scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view ){
        $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
    };

    /* add and removes an event source of choice */
    $scope.addRemoveEventSource = function(sources,source) {
        var canAdd = 0;
        angular.forEach(sources,function(value, key){
            if(sources[key] === source){
                sources.splice(key,1);
                canAdd = 1;
            }
        });
        if(canAdd === 0){
            sources.push(source);
        }
    };
    /* add custom event*/
    $scope.addEvent = function() {
        $scope.events.push({
            title: 'Open Sesame',
            start: new Date(y, m, 28),
            end: new Date(y, m, 29),
            className: ['openSesame']
        });
    };

    var calendar = $("#calendar");
    $scope.uiConfig = {
        calendar: {
            //                     contentHeight: 600,
            //                     aspectRatio: 2,
            editable: false,
            fixedWeekCount:false,
            //                     popover : true,
            displayEventTime: false,
            eventLimit: true, // for all non-agenda views
            views: {
                month: {
                    eventLimit: 7 // adjust to 6 only for agendaWeek/agendaDay
                },
                week: {
                    columnFormat: "ddd DD-MM",            //ddd
                },
                day: {
                    //                            titleFormat: "dddd d MMMM YYYY",
                    columnFormat: "dddd",//"dddd d",           
                }
            },
            header: {
                left: 'prev title next',
                center: '',
                //,right: 'month basicWeek basicDay today',// agendaDay agendaWeek 
            },
            eventClick: $scope.alertOnEventClick,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            eventRender: $scope.eventRender,
            dayClick: $scope.dayClick,
            changeView: $scope.viewRender,
            eventAfterAllRender: $scope.eventAfterAllRender

        }
    };




    /* event sources array*/
    $scope.eventSources =[$scope.events];



    

    var out = [];
    var today ="";
    $scope.updateAppointmentDiv = true;
    $scope.initDashboard = function(){
        today = $filter('date')(new Date(),'yyyy-MM-dd');

    }


    
    $scope.cancelAppointment = function(selected){
       
       //$scope.updateAppointmentDiv = false;
        $("#cancelAppointmentModal").modal("show");
         $("#updateAppointmentModal").modal("hide");
       $window.onload();
       
    }

    


    /*branch*/
    $scope.branches = [{"branchNameCalendar":"Branch 1(branch)"},{"branchNameCalendar":"Branch 2(branch)"},{"branchNameCalendar":"Branch                        3(branch)"},{"branchNameCalendar":"Branch 4(branch)"},{"branchNameCalendar":"Branch 5(branch)"},                                        {"branchNameCalendar":"Branch 6(branch)"},{"branchNameCalendar":"Branch 7(branch)"},{"branchNameCalendar":"Branch                        8(branch)"},{"branchNameCalendar":"Branch 9(branch)"}]

    /*Staff*/
    $scope.staffInCalendar = [{"staffNameCalendar":"Staff 1 (staff)"},
                              {"staffNameCalendar":"Staff 2(staff)"},
                              {"staffNameCalendar":"Staff 3 (staff)"},
                              {"staffNameCalendar":"Staff 4 (staff)"},
                              {"staffNameCalendar":"Staff 5 (staff)"},
                              {"staffNameCalendar":"Staff 6 (staff)"},
                              {"staffNameCalendar":"Staff 7 (staff)"},
                              {"staffNameCalendar":"Staff 8 (staff)"}]

    /*Service*/
    $scope.serviceInCalendar = [{"serviceNameCalendar":"Service 1"},
                                {"serviceNameCalendar":"Service 2"},
                                {"serviceNameCalendar":"Service 3"},
                                {"serviceNameCalendar":"Service 4"}]

    $scope.selectAllBranch = false;

    $scope.checkAllBranches = function (selected) {
        if(selected){
            $scope.branches.map(function(item){
                item.selectBranch =true;
                $scope.callFunction(selected,item);
            })
        }
        else{
            $scope.branches.map(function(item){
                item.selectBranch =false;
                $scope.callFunction(selected,item);
            })
        }

    };

    $scope.selectedBranch = [];
    $scope.callFunction = function(selected,item){

        if(selected){
            var index = $scope.selectedBranch.findIndex(function(x){ return x.branchNameCalendar});
            if(index === -1){
                $scope.selectedBranch.push(item);
                console.log($scope.selectedBranch,"All Checked called");       
            }

        }
        else{
            $scope.selectedBranch.splice(item,1);

        }

    }

    /*Service List In Model*/
    $scope.serviceList = [{"Sname":"service1"},{"Sname":"service2"},{"Sname":"service3"},{"Sname":"service4"}]


}])