angular.module("courseCtrl",[]).controller("courseCtrl",["$scope","$state",function($scope,$state){
    
    $scope.courses = [{"courseName":"XYZ","fee":"200","amidities":"PQRS"}]
    
    $scope.initUpdateCourse = function(){
        $scope.course = $scope.courses[0];
    }
    $scope.clickNewServices=function(){
        $state.go("addCourse");
    }
    $scope.course={};
    $scope.gotToCourseUpdate = function(value){
        $scope.course = $scope.courses[0];
        $state.go("updateCourse");
    }
}])