angular.module("loginCtrl",[]).controller("loginCtrl",["$scope","$window","$log","$state","$rootScope",function($scope,$window,$log,$state,$rootScope){


    $rootScope.user = {"username":"admin","password":"admin"};
    $rootScope.user1 = {"username":"staff","password":"staff"};
    $rootScope.user2 = {"username":"branch","password":"branch"};
    $rootScope.user3 = {"username":"user","password":"user"};


    $scope.onLoginClick=function(loginForm,signinData){

        if($rootScope.user.username == signinData.username && $rootScope.user.password === signinData.password){
            $state.go("admin-dashboard");
            $window.localStorage.role = "admin";
        }
        else if($rootScope.user1.username == signinData.username && $rootScope.user1.password === signinData.password){
            $state.go("admin-dashboard");
            $window.localStorage.role = "staff";
        }
        else if($rootScope.user2.username == signinData.username && $rootScope.user2.password === signinData.password){
            $state.go("admin-dashboard");
            $window.localStorage.role = "branch";
        }
        else if($rootScope.user3.username == signinData.username && $rootScope.user3.password === signinData.password){
            $state.go("admin-dashboard");
            $window.localStorage.role = "user";
        }

    }

}])