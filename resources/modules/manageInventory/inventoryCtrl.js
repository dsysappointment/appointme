angular.module("inventoryCtrl",[]).controller("inventoryCtrl",["$scope","$state",function($scope,$state){
    
    /*--Category--*/
    $scope.categories = [{"name":"XYZ","descriptionName":"This is XYZ Category to use ABC description"}]
    
    $scope.initUpdateCategory=function(){
        $scope.category = $scope.categories[0];
    }
    $scope.clickNewServices = function(){
        $state.go("addCategory");
    }
    $scope.category = {};
    $scope.gotToUpdateCategory = function(value){
        $scope.category = $scope.categories[0];
        $state.go("updateCategory");
    }
    
    /*--Brand--*/
    
      $scope.brandList = [{ "name":"ABC","category":"XYZ","description":"Description"}]
    
    $scope.initUpdateBrand = function(){
        $scope.brand = $scope.brandList[0];
    }
    $scope.clickNewServices = function(){
        $state.go("updateBrand");
    }
    $scope.brand = {};
    $scope.gotToUpdateBrand = function(value){
        $scope.brand = $scope.brandList[0];
        $state.go("updateBrand")
    }
    
    /*--Product--*/
    
     $scope.products = [{"productName":"OUAI Repair Shampoo",
                       "category":"Hair Wash",
                       "brand":"OUAI",
                       "price":"30"},
                      {"productName":"Lorel Hair Shampoo",
                       "category":"Hair Wash","brand":"Lorel","price":"24"}]
    
     $scope.initUpdateProduct = function(){
        $scope.product = $scope.products[0];
    }
    $scope.clickNewServices = function(){
        $state.go("addProduct");
    }
    $scope.product = {};
    $scope.gotToUpdateProduct = function(value){
        $scope.product = $scope.products[0];
        console.log("called")
        $state.go("updateProduct");
    }
    
    /*Purchase Order*/
    
    $scope.purchaseOrder = [{"product":"ABC","quantity":"2","amount":"2000"}]
}])