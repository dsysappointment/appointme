angular.module("navigationCtrl",[]).controller("navigationCtrl",["$scope","$window","$log","$rootScope",function($scope,$window,$log,$rootScope){
    
  // console.log($window.localStorage.role)
   $rootScope.role = $window.localStorage.role;
  
    
    $("#assignment > a").click(function () {
         
           if ($(this).parent("#assignment").hasClass("open")) {
               $(this).parent("#assignment").removeClass("open");
               $(this).parent("#assignment").children("ul").slideUp("slow");
           } else {
               $(this).parent("#assignment").addClass("open");
               $(this).parent("#assignment").children("ul").slideDown("slow");
           };
       });
}])