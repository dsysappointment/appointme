angular.module("branchCtrl",[]).controller("branchCtrl",["$scope","$state",function($scope,$state){
    
     
    $scope.screenTitle="Manage Branch";
    
    $scope.days=["SU","MO","TU","WE","TH","FR","SA"];
    
    $scope.branches = [{"bname":"Central India","openingTime":"2:00pm","closingTime":"10:00pm","mobileNo":"9888298888","address":"City Center"}]

    $scope.branch = {};
    $scope.gotToUpdateBranch=function(value){
        $scope.branch = $scope.branch[0];
        $state.go("updateBranch");
    }
}]);