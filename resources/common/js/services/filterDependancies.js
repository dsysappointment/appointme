angular.module("filterDependancies",[]).service("filterDependancies", ["$http",
    function($http) {
        var filterDependancies = {};
        /*Remove duplicate elements by doctors*/
        filterDependancies.removeDuplicateDoctors = function(arr) {
           console.log(arr,"remove duplicate");
            var newArr = [];
            angular.forEach(arr, function (value, key) {
               
                var exists = false;
                angular.forEach(newArr, function (val2, key) {
                   
                    if (angular.equals(value.resource.user.email, val2.resource.user.email)) {
                        exists = true
                    };
                });
                if (exists == false && value.id != "") {
                    newArr.push(value);
                }
            });
            return newArr;
        }
        
        filterDependancies.removeDuplicateOffers = function(arr) {
            var newArr = [];
            console.log(arr);
            angular.forEach(arr, function (value, key) {
                var exists = false;
                angular.forEach(newArr, function (val2, key) {
                    if (value.status != "COUNTER" && value.status != "FREE") {
                        if (angular.equals(value.status, val2.status)) {
                            if (angular.equals(value.eventData.offerNo, val2.eventData.offerNo)) {
                                if (angular.equals(new Date(value.start._d).getTime(), new Date(val2.start._d).getTime())) {
                                    exists = true;
                                }
                            }
                        }
                    }
                });
                if (exists === false) {
                    newArr.push(value);
                }
            });
            return newArr;
        }
        
        /*Remove duplicate elements by unique_id*/
        filterDependancies.removeDuplicateDates = function(arr, actualVal) {
          
            var newArr = [];
            
            angular.forEach(arr, function (value, key) {
                var exists = false;
                angular.forEach(newArr, function (val2, key) {
                    if (angular.equals(value[actualVal], val2[actualVal])) {
                        exists = true
                    };
                });
                if (exists == false && value.id != "") {
                    newArr.push(value);
                }
            });
            return newArr;
        }
        
        /*Remove duplicate free slots*/
        filterDependancies.removeDuplicateFreeSlots = function(arr) {
            
            for (var i = 0; i < $scope.viewCurrentSlots.length; i++) {
                var offer = $scope.viewCurrentSlots[i];
                for (var j = 0; j < $scope.freeSlots.length; j++) {
                    var freeSlot = $scope.freeSlots[j];
                    if (moment(freeSlot.eventData.startTime, "HH:mm:ss").format("HH:mm") === moment(offer.eventData.startTime, "HH:mm:ss").format("HH:mm")) {
                        if (angular.equals(freeSlot.id, offer.slotId)) {
                            alert(freeSlot.id+" "+ offer.slotId);
                            $scope.freeSlots.splice(j, 1);
                        };
                    }
                }
            }

            return $scope.freeSlots;
        }
        
        /*---------------Remove duplicate elements------------*/
        filterDependancies.removeDuplicate = function(arr) {
            var newArr = [];
            angular.forEach(arr, function (value, key) {
                var exists = false;
                angular.forEach(newArr, function (val2, key) {
                    if (angular.equals(value.id, val2.id) && angular.equals(value.eventData.type, val2.eventData.type)) {
                        exists = true
                    };
                });
                if (exists == false && value.id != "") {
                    newArr.push(value);
                }
            });
            return newArr;
        }
        
        /*Remove duplicate open offers*/
        filterDependancies.removeDuplicateOpenOffers = function(arr) {
            var newArr = [];
            angular.forEach(arr, function (value, key) {
                var exists = false;
                angular.forEach(newArr, function (val2, key) {
                    if (value.status === "OFFERED") {
                        if (angular.equals(value.status, val2.status)) {
                            if (angular.equals(value.offerNo, val2.offerNo)) {
                                exists = true;
                            }
                        }
                    }
                });
                if (exists === false) {
                    newArr.push(value);
                }
            });
            return newArr;
        }
        
        /*Remove duplicate counter offers*/
        filterDependancies.removeDuplicateCounterOffers = function(arr) {
            
            
            
            var newArr = [];
            angular.forEach(arr, function (value, key) {
                var exists = false;
               
                angular.forEach(newArr, function (val2, key) {
                    if (value.status === "COUNTER") {
                        if (angular.equals(value.status, val2.status)) {
                            if (angular.equals(value.offerNo, val2.offerNo)) {
                                exists = true;
                            }
                        }
                    }
                });
                
               
                if (exists === false) {
                 
                    newArr.push(value);
                }
            });
            return newArr;
        }
        
        /*Remove duplicate counter offers by doctor*/
        filterDependancies.removeDuplicateCounterOffersByDoc = function(arr) {
            var newArr = [];
            angular.forEach(arr, function (value, key) {
                var exists = false;
                angular.forEach(newArr, function (val2, key) {
                    if (value.status === "COUNTER") {
                        if (angular.equals(value.doctors[0].resource.id, val2.doctors[0].resource.id)) {
                            exists = true;
                        }
                    }
                });
                if (exists === false) {
                    newArr.push(value);
                }
            });
            return newArr;
        }
        
        /*Remove free slots,offered and counter data-*/
        filterDependancies.removeOffers = function(arr) {
            var newArr = [];
            angular.forEach(arr, function (value, key) {
                var exists = false;
                if (value.status === "FREE" || value.status === "OFFERED" || value.status === "COUNTER") exists = true;
                if (exists == false && value.id != "") {
                    newArr.push(value);
                }
            });
            return newArr;
        }
        
        /*Remove free slots,offered and counter data-*/
        filterDependancies.findDuplicate = function(arr) {
            var newArr = [];
            var dups = false;
            angular.forEach(arr, function (value, key) {
                var exists = false;
                angular.forEach(newArr, function (val2, key) {
                    if (value.status === "FREE") {
                        if (angular.equals(value.date, val2.date)) {
                            if(new Date(value.start._d).getTime()>=new Date(val2.start._d).getTime() && new Date(value.start._d).getTime()<=new Date(val2.end._d).getTime()){
                                exists = true;
                                dups = true;
                            } else if(new Date(value.start._d).getTime()<=new Date(val2.start._d).getTime() && new Date(value.end._d).getTime()>=new Date(val2.end._d).getTime()){
                                exists = true;
                                dups = true;
                            } else if((new Date(value.start._d).getTime()>=new Date(val2.start._d).getTime() && new Date(value.start._d).getTime()<=new Date(val2.end._d).getTime()) && new Date(value.end._d).getTime()>=new Date(val2.end._d).getTime()){
                                exists = true;
                                dups = true;
                            } else {
                                exists = (exists) ? true : false;
                                dups =  (dups) ? true : false;
                            }
                            /*if (angular.equals(value.start._i, val2.start._i)) {
                                if (angular.equals(value.end._i, val2.end._i)) {
                                    exists = true;
                                    dups = true;
                                }
                            }*/
                        }
                    }
                });
                if (exists === false) {
                    newArr.push(value);
                }
            });
            return dups;
        }
        
        /*-------Remove leaves-----------*/
        filterDependancies.removeLeaves = function(arr) {
            var newArr = [];
            angular.forEach(arr, function (value, key) {
                console.log(value);
                var exists = false;
                if (value.status === "LEAVE") 
                    exists = true;
                if (exists == false && value.id != "") {
                    newArr.push(value); /*$scope.viewSlots.splice(key,1);*/
                }
            });
            return newArr;
        }
        
        /*-------Date with day name-----------*/
        var days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
        filterDependancies.dateWithDayName = function(date) {
            var formattedDate = days[moment(new Date(date)).day()] + " " + moment(new Date(date)).format("DD/MM/YYYY");
            return formattedDate;
        }

        filterDependancies.startEndTimeFormat = function(startTime, endTime) {
            var formatedTime = moment(startTime, "HH:mm:ss").format("HH:mm") + " - " + moment(endTime, "HH:mm:ss").format("HH:mm");
            return formatedTime;
        }
                
        return filterDependancies;
    }]);



