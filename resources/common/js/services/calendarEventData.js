angular.module("calendarEventData",[]).service("calendarEventData", ["$http", "$window", "$rootScope",
    function($http,$window, $rootScope) {
        var calendarEventData = {};
        var unique_id = 0;
        var userRole, user;
        
        if (!$window.localStorage.user) {
            $state.go("home");
        } else {
            user = JSON.parse($window.localStorage.user);
        }
        
        if ($window.localStorage.userRole) {
            userRole = $window.localStorage.userRole;
        }
        
        calendarEventData.createEventObject = function(event) {   // calendar rendering data on load 
          //  console.log(angular.toJson(event))
           
         
            var data = {};
         data = {
             id: event.recId, //event.offerId ? event.offerId : event.bookingId,
             color: getBGColor(event.status),
             unique_id: "0" + unique_id++,
             textColor: getTextColor(event.status),
             borderColor: event.status === "LEAVE" ? "#fff" : getBorderColor(event.status),
             stick: true,
             title: getEventTitle(event), //"FMI 08:00 - 12:00 SM $1000",
             start: (event.status === "LEAVE") ? moment(event.startDate + " 00:05:05") : moment(event.startDate + " " + event.startTime),
             end: (event.status === "LEAVE") ? moment(event.endDate + " 23:59:00") : moment(event.endDate + " " + event.endTime),
             siteName: event.site ? event.site.name : "",
             site: event.site ? event.site.name : "",
             status: event.status,
             slotOffers: "Offers",
             doctors: event.doctors,
             //                 doctorName : event.resource.user.lastName +', '+ event.resource.user.firstName,
             eventData: event,
             slotId: event.slotId,
             //                 className : "leave-other"
             className: event.status === "LEAVE" ? "leave" : "",
             allDay : event.status === "LEAVE" ? true : false,
         };
            
            
            
         return data;
     }// go back to the calendarNavigationCtrl.js for further process

     var getEventTitle = function(event) {
         
         console.log(event);
    
         event.offerRate = event.offerRate ? event.offerRate : 0;  
         
         if ($window.localStorage.userRole !== "GROUP_VIEWER") {
             if (event.status === "FREE") {
                 
                 return moment(event.startTime, "HH:mm:ss").format("HH:mm") + " - " + moment(event.endTime, "HH:mm:ss").format("HH:mm");
             } 
             
             else if (event.status === "OFFERED") {
                 
                 return event.site.uniqueId + " " + moment(event.startTime, "HH:mm:ss").format("HH:mm") + " - " + moment(event.endTime, "HH:mm:ss").format("HH:mm") + " " + " $" + event.offerRate;
             } else if (event.status === "COUNTER") {
                 if (event.doctors[0].proposedStartTime && event.doctors[0].counterRate) {
                     return event.site.uniqueId + " " + moment(event.doctors[0].proposedStartTime, "HH:mm:ss").format("HH:mm") + " - " + moment(event.doctors[0].proposedEndTime, "HH:mm:ss").format("HH:mm") + " " + event.doctors[0].resource.user.uniqueId + " $" + event.doctors[0].counterRate;
                 } else if (event.doctors[0].proposedStartTime && !event.doctors[0].counterRate) {
                     return event.site.uniqueId + " " + moment(event.doctors[0].proposedStartTime, "HH:mm:ss").format("HH:mm") + " - " + moment(event.doctors[0].proposedEndTime, "HH:mm:ss").format("HH:mm") + " " + event.doctors[0].resource.user.uniqueId + " $" + event.offerRate;
                 } else if (!event.doctors[0].proposedStartTime && event.doctors[0].counterRate) {
                     return event.site.uniqueId + " " + moment(event.startTime, "HH:mm:ss").format("HH:mm") + " - " + moment(event.endTime, "HH:mm:ss").format("HH:mm") + " " + event.doctors[0].resource.user.uniqueId + " $" + event.doctors[0].counterRate;
                 } else {
                     return event.site.uniqueId + " " + moment(event.startTime, "HH:mm:ss").format("HH:mm") + " - " + moment(event.endTime, "HH:mm:ss").format("HH:mm") + " " + event.doctors[0].resource.user.uniqueId + " $" + event.offerRate;
                 }
             } else if ((event.status === "ROSTERED" || event.status === "COMPLETED" || event.status === "DNA") && $window.localStorage.userRole !== "DOCTOR") {
                 return event.site.uniqueId + " " + moment(event.startTime, "HH:mm:ss").format("HH:mm") + " - " + moment(event.endTime, "HH:mm:ss").format("HH:mm") + " " + event.doctors[0].resource.user.uniqueId + " $" + event.offerRate;
             } else if ((event.status === "ROSTERED" || event.status === "COMPLETED" || event.status === "DNA") && $window.localStorage.userRole === "DOCTOR") {
                 if (event.doctors[0].resource.user.email === JSON.parse(window.localStorage.user).email) {
                        return event.site.uniqueId + " " + moment(event.startTime, "HH:mm:ss").format("HH:mm") + " - " + moment(event.endTime, "HH:mm:ss").format("HH:mm") + " " + event.doctors[0].resource.user.uniqueId + " $" + event.offerRate;    
                 } else {
                     return event.site.uniqueId + " " + moment(event.startTime, "HH:mm:ss").format("HH:mm") + " - " + moment(event.endTime, "HH:mm:ss").format("HH:mm") + " " + event.doctors[0].resource.user.uniqueId;
                 }
             } else if (event.status === "CLOSED") {
                 return event.site.uniqueId + " " + moment(event.startTime, "HH:mm:ss").format("HH:mm") + " - " + moment(event.endTime, "HH:mm:ss").format("HH:mm") + " CLOSED";
             } else if (event.status === "LEAVE") {
                 if (!angular.isUndefined(event.doctors) && event.doctors.length) {
                     if (event.doctors[0].resource.user.email === JSON.parse($window.localStorage.user).email) {
                         return event.doctors[0].resource.user.uniqueId;//"LEAVE";
                     } else {
                         var leavetitle = [];
                         if (!$rootScope.MISView) {
                             for (var i = 0; i < event.doctors.length; i++) {
                                 if (event.doctors[i].resource.user.email != JSON.parse($window.localStorage.user).email) {
                                     var data = event.doctors[i].resource.user.uniqueId;
                                     leavetitle.push(data);
                                 }
                             }
                             return leavetitle.toString();
                         } else {
                             var data = event.doctors[0].resource.user.uniqueId;
                             return data;//"LEAVE";
                         }
                     }
                 } else {
                     if (event.resource.user.email === JSON.parse($window.localStorage.user).email) {
                         return event.resource.user.uniqueId;
//                                 return "LEAVE";
                     } else {
                         return event.resource.user.uniqueId;
                     }
                 }
             }
         }
         
         //for group viewer 
         else {
             if (event.status === "FREE") {
                
                 return moment(event.startTime, "HH:mm:ss").format("HH:mm") + " - " + moment(event.endTime, "HH:mm:ss").format("HH:mm");
             } else if (event.status === "OFFERED") {
                 return event.site.uniqueId + " " + moment(event.startTime, "HH:mm:ss").format("HH:mm") + " - " + moment(event.endTime, "HH:mm:ss").format("HH:mm");
             } else if (event.status === "COUNTER") {
                 if (event.doctors[0].proposedStartTime && event.doctors[0].counterRate) {
                     return event.site.uniqueId + " " + moment(event.doctors[0].proposedStartTime, "HH:mm:ss").format("HH:mm") + " - " + moment(event.doctors[0].proposedEndTime, "HH:mm:ss").format("HH:mm") + " " + event.doctors[0].resource.user.uniqueId;
                 } else if (event.proposedStartTime && !event.counterRate) {
                     return event.site.uniqueId + " " + moment(event.doctors[0].proposedStartTime, "HH:mm:ss").format("HH:mm") + " - " + moment(event.doctors[0].proposedEndTime, "HH:mm:ss").format("HH:mm") + " " + event.doctors[0].resource.user.uniqueId;
                 } else if (!event.proposedStartTime && event.counterRate) {
                     return event.site.uniqueId + " " + moment(event.startTime, "HH:mm:ss").format("HH:mm") + " - " + moment(event.endTime, "HH:mm:ss").format("HH:mm") + " " + event.doctors[0].resource.user.uniqueId;
                 } else {
                     return event.site.uniqueId + " " + moment(event.startTime, "HH:mm:ss").format("HH:mm") + " - " + moment(event.endTime, "HH:mm:ss").format("HH:mm") + " " + event.doctors[0].resource.user.uniqueId;
                 }
             } else if (event.status === "ROSTERED" || event.status === "COMPLETED" || event.status === "DNA") {
                 return event.site.uniqueId + " " + moment(event.startTime, "HH:mm:ss").format("HH:mm") + " - " + moment(event.endTime, "HH:mm:ss").format("HH:mm") + " " + event.doctors[0].resource.user.uniqueId;
             } else if (event.status === "CLOSED") {
                 return event.site.uniqueId + " " + moment(event.startTime, "HH:mm:ss").format("HH:mm") + " - " + moment(event.endTime, "HH:mm:ss").format("HH:mm") + " CLOSED";
             } else if (event.status === "LEAVE") {
                 if (!angular.isUndefined(event.doctors) && event.doctors.length) {
                     if (event.doctors[0].resource.user.email === JSON.parse($window.localStorage.user).email) {
                         return event.doctors[0].resource.user.uniqueId;//"LEAVE";
                     }
                     
                     else {
                         var leavetitle = [];
                         if (!$rootScope.MISView) {
                             for (var i = 0; i < event.doctors.length; i++) {
                                 if (event.doctors[i].resource.user.email != user.email) {
                                     var data = event.doctors[i].resource.user.uniqueId;
                                     leavetitle.push(data);
                                 }
                             }
                             return leavetitle.toString();
                         } else {
                             var data = event.doctors[0].resource.user.uniqueId;
                             return data;//"LEAVE";
                         }
                     }
                 } else {
                     if (event.resource.user.email === JSON.parse($window.localStorage.user).email) {
                         return event.resource.user.uniqueId;//"LEAVE";
                     } else {
                         return event.resource.user.uniqueId;
                     }
                 }
             }
         }
     }

     var getBGColor = function(status) {
         if (status === "OFFERED" || status === "COUNTER") {
             return "#fbbc05";
         } else if (status === "ROSTERED") {
             return "#fff";
         } else if (status === "COMPLETED") {
             return "#fff";
         } else if (status === "DNA") {
             return "#fff";
         } else if (status === "CLOSED") {
             return "#fff";
         } else if (status === "UNAVAILABLE" || status === "REJECTED") {
             return "#fff";
         } else if (status === "FREE") {
             return "#dad6d6";
         } else if (status === "LEAVE") {
             return "#fff";
         }
     }

     var getTextColor = function(status) {
         if (status === "OFFERED" || status === "COUNTER") {
             return "#000";
         } else if (status === "ROSTERED") {
             return "#34a853";
         } else if (status === "COMPLETED") {
             return "#5390f6";
         } else if (status === "DNA") {
             return "#000";
         } else if (status === "CLOSED") {
             return "#fd8725";
         } else if (status === "UNAVAILABLE" || status === "REJECTED") {
             return "#fff";
         } else if (status === "FREE") {
             return "#000";
         } else if (status === "LEAVE") {
             return "#e42c0f";
         }

     }

     var getBorderColor = function(status) {
         if (status === "OFFERED" || status === "COUNTER") {
             return "rgba(0, 0, 0,0.2)";

         } else if (status === "ROSTERED") {
             return "rgba(0, 153, 0,0.2)";
         } else if (status === "COMPLETED") {
             return "rgba(0, 102, 255,0.2)";
         } else if (status === "DNA") {
             return "rgba(0, 0, 0,0.2)";
         } else if (status === "CLOSED") {
             return "rgba(255, 128, 0,0.2)";
         } else if (status === "UNAVAILABLE" || status === "REJECTED") {
             return "#fff";
         } else if (status === "FREE") {
             return "rgba(0, 153, 0,0.2)";
         } else if (status === "LEAVE") {
             return "rgba(255, 64, 0,0.2)";
         }

     }
        return calendarEventData;
    }]);